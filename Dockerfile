FROM debian:stable-slim

LABEL maintainer="Jonas Chopin-Revel" email="contact@jonas-chopin.com" description="Simple toolbox for debug based on Debian stable"

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get dist-upgrade -y && \
    apt-get install -o APT::Install-Suggests=0 -o APT::Install-Recommends=0 -y \
    ca-certificates \
    curl \
    dnsutils \
    gnutls-bin \
    gpg \
    hping3 \
    htop \
    iputils-ping \
    jq \
    lsof \
    mtr-tiny \
    ncdu \
    netcat-traditional \
    nethogs \
    net-tools \
    nmap \
    openssh-client \
    openssl \
    procps \
    redis-tools \
    rsync \
    tcpdump \
    vim-tiny \
    wget \
    zip && \
    apt-get clean -y && \
    apt-get autoclean -y && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/* /tmp/* /var/tmp/*

CMD ["/bin/bash"]
