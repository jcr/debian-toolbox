# Docker - Debian Toolbox

It's a simple Docker image based on Debian which contents some additional packages. It mainly aims to get quickly a Debian container with useful tools to debug.

Current version: 1.2.0

Hubdocker: https://hub.docker.com/r/jonaschopin/debian-toolbox

Usage:
```
docker pull jonaschopin/debian-toolbox
```

Installed packages:
 * ca-certificates
 * curl
 * dnsutils
 * gnutls-bin
 * gpg
 * hping3
 * htop
 * iputils-ping
 * jq
 * lsof
 * mtr-tiny
 * ncdu
 * netcat-traditional
 * nethogs
 * net-tools
 * nmap
 * openssh-client
 * openssl
 * procps
 * redis-tools
 * rsync
 * tcpdump
 * vim-tiny
 * wget
 * zip

Tool developed by Jonas Chopin-Revel on License GNU-GPLv3.

Gitlab : https://framagit.org/jcr/debian-toolbox

**Share and improve it. It's free !**
